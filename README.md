# PJL Anti-gaspi Front

_Web front-end to track the legislative progress of the [French anti-waste bill](https://www.ecologique-solidaire.gouv.fr/loi-anti-gaspillage)_

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/pjl-anti-gaspi/pjl-anti-gaspi-front

## Install

```bash
git clone https://framagit.org/pjl-anti-gaspi/pjl-anti-gaspi-front.git
cd pjl-anti-gaspi-front/
```

Create a `.env` file to customize configuration (you can use `example.env` as a template). Then

```bash
npm install
```

## Server Launch

In development mode:

```bash
npm run dev
```

In production mode:

```bash
npm run build
npm start
```
