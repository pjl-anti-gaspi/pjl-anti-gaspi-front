# PJL Anti-gaspi Front

_Web front-end to track the legislative progress of the [French anti-waste bill](https://www.ecologique-solidaire.gouv.fr/loi-anti-gaspillage)_

By: Emmanuel Raviart <mailto:emmanuel@raviart.com>

Copyright (C) 2019 Emmanuel Raviart

https://framagit.org/pjl-anti-gaspi/pjl-anti-gaspi-front

> _PJL Anti-gaspi Front_ is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> _PJL Anti-gaspi Front_ is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
